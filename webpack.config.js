/* global process __dirname */

const path           = require('path');
const fs             = require('fs');
const webpack        = require('webpack');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const context        = path.join(__dirname);
const npmConfig      = JSON.parse(fs.readFileSync('./package.json')).config;

const inProduction = process.env.NODE_ENV === 'production';
const env = inProduction ? 'production' : 'development';

const config = {
  context: context
};

config.mode = env;

config.watch = false;

config.module = {
  rules: [
    {
      test: /\.jsx?$/,
      // Don't exclude modules from being transformed as many are now written in es6
      //exclude: /(node_modules|bower_components)/,
      loader: 'babel-loader',
      options: {
        babelrc: false,
        presets: [
          ['env', {
            targets: {
              browsers: ['last 4 versions', 'safari >= 7']
            },
            modules: false
          }]
        ],
      }
    },
    {
      test: /\.tsx?$/,
      // exclude: /(node_modules|bower_components)/,
      use: [
        {
          loader: 'babel-loader',
          options: {
            babelrc: false,
            presets: [
              ['env', {
                targets: {
                  browsers: ['last 4 versions', 'safari >= 7']
                },
                modules: false
              }]
            ],
          }
        },
        {
          loader: 'ts-loader'
        }
      ]
    }
  ]
};

config.resolve = {
  modules: [
    path.resolve(`${npmConfig.assetSource}scripts`),
    'node_modules',
    'bower_components'
  ],
  alias: {
    'vue$': 'vue/dist/vue.esm.js'
  },
  extensions: ['.js', '.jsx', '.ts', '.tsx', '.vue', '.json']
};


config.externals ={
  'jquery': 'jQuery'
};

config.devtool = inProduction ? '#source-map' : '#eval-source-map';

const providePlugin = new webpack.ProvidePlugin({
    $: 'jquery',
    jQuery: 'jquery',
    'window.jQuery': 'jquery'
});

const envProcess = new webpack.DefinePlugin({
  'process.env.NODE_ENV': JSON.stringify(env)
});

const uglifyPlugin = new UglifyJSPlugin({
  uglifyOptions: {
    output: {
      comments: false
    },
    compress: {
      unsafe_comps: true,
      properties: true,
      keep_fargs: false,
      pure_getters: true,
      collapse_vars: true,
      unsafe: true,
      warnings: false,
      sequences: true,
      dead_code: true,
      drop_debugger: true,
      comparisons: true,
      conditionals: true,
      evaluate: true,
      booleans: true,
      loops: true,
      unused: true,
      hoist_funs: true,
      if_return: true,
      join_vars: true,
      drop_console: true
    }
  }
})


config.optimization = {
  minimizer: []
};


const loaderOptions = new webpack.LoaderOptionsPlugin({
  minimize: true
});

const aggressiveMerging = new webpack.optimize.AggressiveMergingPlugin();

config.plugins = [
  providePlugin,
  envProcess
];


if (inProduction) {
  config.plugins.push( uglifyPlugin, aggressiveMerging, loaderOptions );
  config.optimization.minimizer.push(uglifyPlugin);
}

module.exports = config;
