<?php

namespace Techmark\Controllers;

use Timber\Timber;

class BaseController
{

	protected $context;

	function __construct()
	{

		$this->context = Timber::get_context();
		
		$this->context['menu'] = new \Timber\Menu( 'Header Menu' );
		$this->context['footerMenu'] = new \Timber\Menu( 'Footer Menu' );
		
		$this->context['options'] = get_fields('option');
		

		/*

		*** Add to global Timber context ***

		$this->context['main_menu'] = new Timber\Menu( 'main-menu' );

		*/

	}

}