<?php

namespace Techmark\Controllers;

use Timber\Timber;
use Techmark\Helpers\Route;

class PageController extends BaseController
{

	function __construct()
	{

		parent::__construct();

		/*

		*** Add to controller-wide Timber context ***

		*/


	}

	public function showPage()
	{

		$this->context['page'] = Timber::get_post();
		Route::view( 'page.twig', $this->context );

	}


	public function showKeyStaffPage()
	{

		$this->context['staffPage'] = Timber::get_post();
		$this->context['staff'] = Timber::get_posts(array('post_type' => 'staff', 'posts_per_page' => 12, 'orderby' => 'date', 'order' => 'desc'));
		Timber::render( 'page.keyStaff.twig', $this->context );

	}

	public function showStaffSingle()
	{

		$this->context['sfaffSingle'] = Timber::get_post();
		Route::view( 'single.staff.twig', $this->context );
	}

	public function showProperty()
	{

		$this->context['property'] = Timber::get_post();
		Route::view( 'single.property.twig', $this->context );
	}

	public function showContactPage()
	{

		$this->context['contact'] = Timber::get_post();
		Timber::render( 'page.contact.twig', $this->context );

	}


	public function showHomePage()
	{
		$this->context['page'] = Timber::get_post();
		$this->context['properties'] = Timber::get_posts(array('post_type' => 'property', 'posts_per_page' => 3, 'orderby' => 'date', 'order' => 'desc'));
		Route::view( 'page.home.twig', $this->context );
	}



	public function showRentPage()
	{
		global $paged;
		 if (!isset($paged) || !$paged){
		     $paged = 1;
		 }
	     $this->context['page'] = Timber::get_post();
	     $args = array(
	         'post_type' => 'property',
	         'posts_per_page' => 12,
	         'paged' => $paged
	     );
	     $this->context['properties'] = new \Timber\PostQuery($args);



			$this->context['rent'] = Timber::get_post();
			Route::view( 'page.rent.twig', $this->context );
	}




	public function showError()
	{

		Route::view('404.twig', $this->context);

	}

}