
function sortPropsByDescending() {
  const divList = $(".listing-item");
  divList.sort(function(a, b){ return $(b).data("prop")-$(a).data("prop")});
  $("#list").html(divList);
}

function sortPropsByAscending() {
  const divList = $(".listing-item");
  divList.sort(function(a, b){ return $(a).data("prop")-$(b).data("prop")});
  $("#list").html(divList);
}


$("#sort-props").on('click', function(e) {
  const el = e.currentTarget;
  return (el.tog^=1) ? sortPropsByDescending() : sortPropsByAscending();
});
