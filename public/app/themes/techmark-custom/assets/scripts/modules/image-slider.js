import 'owl.carousel';

const $imageSliders = $("#image-slider");

$imageSliders.owlCarousel( {
  loop:true,
  autoplay:true,
  nav:false,
  dots: true,
  autoplayTimeout:5000,
  responsiveClass:true,
  responsive:{
    0:{
      items:1
    }
  }
});
