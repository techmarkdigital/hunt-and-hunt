<?php

use Techmark\Helpers\Route;


//Route::set( is_post_type_archive('post-type'), 'PostTypeController@showArchive' );

//Route::set( is_singular('post-type'), 'PostTypeController@showSingle' );

//Route::set( is_page_template('test'), 'PageController@showTest' );
Route::set( is_page('key-staff'), 'PageController@showKeyStaffPage' );
Route::set( is_page('contact'), 'PageController@showContactPage' );
Route::set( is_page('rent'), 'PageController@showRentPage' );
Route::set( is_front_page(), 'PageController@showHomePage' );
Route::set( is_page(), 'PageController@showPage' );
Route::set( is_singular('staff'), 'PageController@showStaffSingle' );
Route::set( is_singular('property'), 'PageController@showProperty' );
Route::set( is_404(), 'PageController@showError' );


/*
 * Available conditionals can be found at https://codex.wordpress.org/Conditional_Tags
 * */
